<?php

namespace App\Http\Controllers;
use App\WorkList;
use App\HumanResource;
use Illuminate\Http\Request;
use Session;
use DB;
class HumanResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::flash('Token','Login');
        $blog = HumanResource::all();                
        return view('allhr')->with('hr',$blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       return redirect('/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function edit(HumanResource $humanResource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HumanResource $humanResource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HumanResource  $humanResource
     * @return \Illuminate\Http\Response
     */
    public function destroy(HumanResource $humanResource)
    {
        //
    }
}
