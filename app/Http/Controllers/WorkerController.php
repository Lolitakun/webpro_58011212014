<?php

namespace App\Http\Controllers;

use App\Worker;
use App\WorkList;
use App\HumanResource;
use App\Admin;
use Illuminate\Http\Request;
use DB;
use Session;
class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Worker::all();
        return view('allworker')->with('worker',$data);
    }

    public function  check_login(Request $request){
        
        $username = $request->input('Username');
        $password = $request->input('Password');

        $WORKER = DB::select('select w_password from workers where w_username = ? ',[$username]);
        
        $HR = DB::select('select hr_id,hr_password from human_resources where hr_username = ? ',[$username]);
        $Admin = DB::select('select ad_password from admins where ad_username = ? ',[$username]);

        if(isset($WORKER[0])){
            if($WORKER[0]->w_password == $password){
                Session::flash('Token','Login');
                $blog = WorkList::all();                
                return view('welcome_worker')->with('blog',$blog);
            }           
        } 
        else if(isset($HR[0]))  {
            if($HR[0]->hr_password == $password){
                Session::flash('Token','Login');                
                $blog = WorkList::all();         
                return view('welcome_hr')->with('blog',$blog)->with('owner',$HR[0]->hr_id);
            }
        }   
        else if(isset($Admin[0])){            
            if($Admin[0]->ad_password == $password){
                Session::flash('Token','Login'); 
                $blog = WorkList::all();                        
                return view('welcome_admin')->with('blog',$blog);
            }
        }  
        else{
            Session::flash('message','Sorry invalid username and password.');
            return view('welcome');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->status == 'Ee'){
            //insert to Worker
            $wk = new Worker();
            $wk->w_username = $request->Username;
            $wk->w_password = $request->Password;
            $wk->w_fullname = $request->Fullname;
            $wk->w_email = $request->email;
            $wk->w_tel = $request->tel;
            $wk->w_address = $request->address;
            $wk->w_status = '0';         
            $wk->w_resume = 'https://img.resume.com/templates/5.png';   
            $wk->w_description = $request->Description;
            $wk->w_Education = $request->Education;

            if($request->hasFile('cover_image')){
                $filenameWithExt = $request->file('cover_image')->getClientOriginalName();           
                $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);
                $extension = $request->file('cover_image')->getClientOriginalExtension();
                $filenametoStore = $filename.'_'.time().'.'.$extension;
                $path = $request->file('cover_image')->storeAs('public/cover_image',$filenametoStore);
                $wk->w_picture = $filenametoStore;
                if($wk->save()){
                    return redirect('/');    
                }           
            }
            else{
                return 'Sorry please try again.';
            }
        }
        else{
            //insert to Hr
            $hr = new HumanResource();
            $hr->hr_username = $request->Username;
            $hr->hr_password = $request->Password;
            $hr->hr_fullname = $request->Fullname;
            $hr->hr_email = $request->email;
            $hr->hr_tel = $request->tel;
            $hr->hr_address = $request->address;                        
            $hr->hr_Description = $request->Description;
            $hr->hr_picture = 'https://img.resume.com/templates/5.png';
            if($hr->save()){
                return redirect('/');    
            }
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function edit(Worker $worker)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worker $worker)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
        Session::flush();
        Session::flash('message','welcome');
        $blog = WorkList::all();
        $data = array(
            'blog'=> $blog
        );
        return view('welcome',$data);
    }
}
