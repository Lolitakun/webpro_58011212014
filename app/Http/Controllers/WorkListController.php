<?php

namespace App\Http\Controllers;

use App\WorkList;
use App\Worker;
use Session;
use DB;
use Illuminate\Http\Request;

class WorkListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::flash('message','welcome');
        $blog = WorkList::all();                
        return view('welcome')->with('blog',$blog);
    }
    public function show_admin(){
        Session::flash('Token','Login');
        $blog = WorkList::all();                
        return view('welcome_admin')->with('blog',$blog);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $list = new WorkList();
        $id = DB::select('select hr_id from human_resources where hr_username = ? ',[$request->Username]);
        $list->wl_owner = $id[0]->hr_id;
        $list->wl_head = $request->Head;
        $list->wl_Education = $request->Education;
        $list->wl_Saraly = $request->Saraly;
        $list->wl_Organization = $request->Organization;
        if($list->save()){
            
            return redirect('/');
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workdata = DB::select('select * from work_lists where wl_id = ?',[$id]);        
        $owner_post = DB::select('select * from human_resources where hr_id = ?',[$workdata[0]->wl_owner]);        
        return view('show_detail')->with('work',$workdata)->with('owner',$owner_post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkList $workList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkList  $workList
     * @return \Illuminate\Http\Response
     */
    public function destroy($wl_id)
    {
        $data = WorkList::find($wl_id);
        $data->delete();
        WorkList::destroy($wl_id);
        return redirect('/login');     
    }
}
