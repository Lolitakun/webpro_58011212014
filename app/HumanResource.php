<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HumanResource extends Model
{
    protected $table = "human_resources";
    protected $fillable = ['hr_username','hr_password','hr_fullname','hr_email','hr_tel','hr_address','hr_Description','hr_picture'];
}
