<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
	protected $table = "workers";
    protected $fillable = ['w_username','w_password','w_fullname','w_email','w_tel','w_address','w_status','w_resume','w_Description','w_picture','w_Education'];
}
