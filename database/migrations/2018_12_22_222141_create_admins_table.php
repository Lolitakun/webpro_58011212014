<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('ad_id');
            $table->string('ad_username')->nullable()->unique();
            $table->string('ad_password')->nullable();
            $table->string('ad_fullname')->nullable();

            $table->string('ad_email')->nullable()->unique();
            $table->string('ad_tel')->unique();          
             
            $table->string('ad_picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
