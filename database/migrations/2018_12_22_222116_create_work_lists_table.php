<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_lists', function (Blueprint $table) {
            $table->increments('wl_id');
            $table->integer('wl_owner')->refernces('hr_id')->on('human_resources');
            $table->string('wl_head')->nullable();
            $table->string('wl_Education')->nullable();
            $table->integer('wl_Saraly')->nullable();
            $table->string('wl_Organization')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_lists');
    }
}
