<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHumanResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('human_resources', function (Blueprint $table) {
            $table->increments('hr_id');
            $table->string('hr_username')->nullable()->unique();
            $table->string('hr_password')->nullable();
            $table->string('hr_fullname')->charset('utf8');

            $table->string('hr_email')->nullable()->unique();
            $table->string('hr_tel')->nullable()->unique();
            
            $table->string('hr_address')->nullable();            
            $table->string('hr_Description');
            $table->string('hr_picture');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('human_resources');
    }
}
