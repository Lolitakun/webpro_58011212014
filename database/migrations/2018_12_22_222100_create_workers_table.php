<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->increments('w_id');
            $table->string('w_username')->nullable()->unique();
            $table->string('w_password')->nullable();
            $table->string('w_fullname')->charset('utf8');

            $table->string('w_email')->unique()->nullable();
            $table->string('w_tel')->unique()->nullable();
            
            $table->string('w_address')->nullable();
            $table->boolean('w_status')->nullable();
            $table->string('w_resume');
            $table->string('w_Description');
            $table->string('w_picture');
            $table->string('w_Education')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
