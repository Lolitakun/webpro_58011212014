<?php

use Illuminate\Database\Seeder;

class HumanResource extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('human_resources')->insert([
            'hr_username' => 'pattananong',
            'hr_password' => '45678901',
            'hr_fullname' => 'Pattanapong Chompoowiset',
            'hr_email' => 'pattanapong.cho@msu.ac.th',
            'hr_tel' => '0812340120',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Jarad',
            'hr_password' => '45678901',
            'hr_fullname' => 'Jarad Chompoowiset',
            'hr_email' => 'Jarad@msu.ac.th',
            'hr_tel' => '0811236520',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Kaelyn',
            'hr_password' => '45788901',
            'hr_fullname' => 'Kaelyn Chompoowiset',
            'hr_email' => 'Kaelyn@msu.ac.th',
            'hr_tel' => '0801235520',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Krista',
            'hr_password' => '45678901',
            'hr_fullname' => 'Krista Chompoowiset',
            'hr_email' => 'Krista@msu.ac.th',
            'hr_tel' => '0801236501',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Laila',
            'hr_password' => '45600901',
            'hr_fullname' => 'Laila Chompoowiset',
            'hr_email' => 'Laila@msu.ac.th',
            'hr_tel' => '0870121023',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Latisha',
            'hr_password' => '564123',
            'hr_fullname' => 'Latisha Chompoowiset',
            'hr_email' => 'Latisha@msu.ac.th',
            'hr_tel' => '08012023620',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Lina',
            'hr_password' => '4575823',
            'hr_fullname' => 'Lina Chompoowiset',
            'hr_email' => 'Lina@msu.ac.th',
            'hr_tel' => '0801232012',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Malinda',
            'hr_password' => '78547523',
            'hr_fullname' => 'Malinda Chompoowiset',
            'hr_email' => 'Malinda@msu.ac.th',
            'hr_tel' => '0801236520',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Marni',
            'hr_password' => '5474523',
            'hr_fullname' => 'Marni Chompoowiset',
            'hr_email' => 'Marni@msu.ac.th',
            'hr_tel' => '0812023698',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
DB::table('human_resources')->insert([
            'hr_username' => 'Marcita',
            'hr_password' => '78657523',
            'hr_fullname' => 'Marcita Chompoowiset',
            'hr_email' => 'Marcita@msu.ac.th',
            'hr_tel' => '0812020369',
            'hr_address' => '123 xxx xxxx xxxxx xxxxx xxxx 33314',    
            'hr_Description' => 'Hello I am Joe Teacher MSU',
            'hr_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',               
        ]);
    }
}
