<?php

use Illuminate\Database\Seeder;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'ad_username' => 'root',
            'ad_password' => 'toor001',
            'ad_fullname' => 'Asanee Nuttawut',
            'ad_email' => 'asanee_nuttawut@msu.ac.th',
            'ad_tel' => '080704949',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Athena',
            'ad_password' => '123423',
            'ad_fullname' => 'Athena Nuttawut',
            'ad_email' => 'Athena@msu.ac.th',
            'ad_tel' => '084568001',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Arista',
            'ad_password' => '567889',
            'ad_fullname' => 'Arista Nuttawut',
            'ad_email' => 'Arista@msu.ac.th',
            'ad_tel' => '080001253',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Aria',
            'ad_password' => '912378',
            'ad_fullname' => 'Aria Nuttawut',
            'ad_email' => 'Aria@msu.ac.th',
            'ad_tel' => '0845750025',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Chloe',
            'ad_password' => '456778',
            'ad_fullname' => 'Chloe Nuttawut',
            'ad_email' => 'Chloe@msu.ac.th',
            'ad_tel' => '0807057810',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Camilla',
            'ad_password' => '891285',
            'ad_fullname' => 'Camilla Nuttawut',
            'ad_email' => 'Camilla@msu.ac.th',
            'ad_tel' => '080700214',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Freya',
            'ad_password' => '000078',
            'ad_fullname' => 'Freya Nuttawut',
            'ad_email' => 'Freya@msu.ac.th',
            'ad_tel' => '080709630',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Grace',
            'ad_password' => '345689',
            'ad_fullname' => 'Grace Nuttawut',
            'ad_email' => 'Grace@msu.ac.th',
            'ad_tel' => '08077820',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Isabella',
            'ad_password' => '789125',
            'ad_fullname' => 'Isabella Nuttawut',
            'ad_email' => 'Isabella@msu.ac.th',
            'ad_tel' => '080707826',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
DB::table('admins')->insert([
            'ad_username' => 'Gracie',
            'ad_password' => '234589',
            'ad_fullname' => 'Gracie Nuttawut',
            'ad_email' => 'Gracie@msu.ac.th',
            'ad_tel' => '080704562',
            'ad_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',           
        ]);
    }
}
