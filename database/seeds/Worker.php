<?php

use Illuminate\Database\Seeder;

class Worker extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workers')->insert([
            'w_username' => 'toey',
            'w_password' => '123456',
            'w_fullname' => 'Asanee Nuttawut',
            'w_email' => 'teerapat@msu.ac.th',
            'w_tel' => '0878941000',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Aileen',
            'w_password' => '123996',
            'w_fullname' => 'Aileen Nuttawut',
            'w_email' => 'Aileen@msu.ac.th',
            'w_tel' => '0845210695',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Alisa',
            'w_password' => '456789',
            'w_fullname' => 'Alisa Nuttawut',
            'w_email' => 'Alisa@msu.ac.th',
            'w_tel' => '0812302369',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Arnon',
            'w_password' => '012345',
            'w_fullname' => 'Arnon Nuttawut',
            'w_email' => 'Arnon@msu.ac.th',
            'w_tel' => '0898563206',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Amara',
            'w_password' => '678901',
            'w_fullname' => 'Amara Nuttawut',
            'w_email' => 'Amara@msu.ac.th',
            'w_tel' => '08021026578',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Dorothy',
            'w_password' => '234567',
            'w_fullname' => 'Dorothy Nuttawut',
            'w_email' => 'Dorothy@msu.ac.th',
            'w_tel' => '0875621530',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Dan',
            'w_password' => '234567',
            'w_fullname' => 'Dan Nuttawut',
            'w_email' => 'Dan@msu.ac.th',
            'w_tel' => '0878920456',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Emmalee',
            'w_password' => '45678901',
            'w_fullname' => 'Emmalee Nuttawut',
            'w_email' => 'Emmalee@msu.ac.th',
            'w_tel' => '0812359875',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Guy',
            'w_password' => '45678901',
            'w_fullname' => 'Guy Nuttawut',
            'w_email' => 'Guy@msu.ac.th',
            'w_tel' => '0878945628',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Scienc',          
        ]);
 DB::table('workers')->insert([
            'w_username' => 'Harlan',
            'w_password' => '45678901',
            'w_fullname' => 'Harlan Nuttawut',
            'w_email' => 'Harlan@msu.ac.th',
            'w_tel' => '0807050581',
            'w_address' => '261 xxx xxxx xxxxx xxxxx 45900',
            'w_status' => '0',
            'w_resume' => 'resume/Teerapat_resume.jpg',
            'w_Description' => 'Hello I am toey, Full-Stack Developer',
            'w_picture' => 'https://www.readersdigest.ca/wp-content/uploads/sites/14/2011/01/4-ways-cheer-up-depressed-cat.jpg',
            'w_Education' => 'Bachelor of Science',         
        ]);
    }
}
