<?php

use Illuminate\Database\Seeder;

class WorkList extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('work_lists')->insert([
            'wl_owner' => '1',
            'wl_head' => '  Siam Agro Industry Pineapple And Others Co.,Ltd.',
            'wl_Education' => 'Doctor of Computer Science',
            'wl_Saraly' => '120000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '2',
            'wl_head' => 'Sahakol Service Center Co.,Ltd.',
            'wl_Education' => 'carpenter',
            'wl_Saraly' => '120000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '3',
            'wl_head' => 'Hong Maobiochemicals Co., Ltd.',
            'wl_Education' => 'soldier',
            'wl_Saraly' => '80000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '4',
            'wl_head' => 'American Standard B & K (Thailand) Public Company Limited',
            'wl_Education' => 'accountant',
            'wl_Saraly' => '5000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '5',
            'wl_head' => 'Eastplast Co.,Ltd.',
            'wl_Education' => 'doctor',
            'wl_Saraly' => '9990',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '6',
            'wl_head' => 'Eoc Polymers (Thailand) Co.,Ltd.',
            'wl_Education' => 'farmer',
            'wl_Saraly' => '125000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '7',
            'wl_head' => 'A-Tek (Material) Co.,Ltd.',
            'wl_Education' => 'singer',
            'wl_Saraly' => '60000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '9',
            'wl_head' => 'Egco Cogeneration Co.,Ltd.',
            'wl_Education' => 'dancer',
            'wl_Saraly' => '90000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '10',
            'wl_head' => 'Lim Thai Heng Plastic Ltd., Part.',
            'wl_Education' => 'cook',
            'wl_Saraly' => '110000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
  DB::table('work_lists')->insert([
            'wl_owner' => '11',
            'wl_head' => 'Sung In Electronics (Thailand) Co.,Ltd.',
            'wl_Education' => 'barber',
            'wl_Saraly' => '1200000',
            'wl_Organization' => '777/49 MakePlus Co.td xxxx xxxx xxxxx xxxxx 483940 pattanapong.cho@msu.ac.th Tel.080705059 ',
        ]);
    }
}
