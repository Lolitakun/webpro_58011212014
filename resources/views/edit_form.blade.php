<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
{{ Html::style(('css/bootstrap.css')) }}
    {{ Html::script('js/jquery-3.3.1.min.js') }}
    {{ Html::script('js/boostrap.min.js') }}
<style>
    .button {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }

</style>
<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
        background-color: black;
    }

    * {
        box-sizing: border-box;
    }

    /* Add padding to containers */
    .container {
        padding: 16px;
        background-color: white;
    }

    /* Full-width input fields */
    input[type=text], input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    /* Overwrite default styles of hr */
    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    /* Set a style for the submit button */
    .registerbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    .registerbtn:hover {
        opacity: 1;
    }

    /* Add a blue text color to links */
    a {
        color: dodgerblue;
    }

    /* Set a grey background color and center the text of the "sign in" section */
    .signin {
        background-color: #f1f1f1;
        text-align: center;
    }
</style>


<body>

<div class="w3-container w3-green">
    <h1>W3Schools Demo</h1>
    <p>Resize this responsive page!</p>
</div>
    <div class="container">
        <h1>Edit Eat</h1>
        <p>Please fill in this.</p>
        <hr>        
        {{ Form::open(['action' => 'WorkListController@update', 'method'=>'POST','enctype'=> 'multipart/form-data']) }}
        	<label for="emain"><b>email</b></label>
        	{{ Form::text('email','',['class' => 'form-control']) }}

	        <label for="psw"><b>Password</b></label>
	        {{ Form::password('Password') }}
	        
	        <label for="fullname"><b>Full Name</b></label>
        	{{ Form::text('Fullname','',['class' => 'form-control']) }}
        <br>
        <h2>Personal Information</h2>
        <br>
	        <label for="tel"><b>tel</b></label>
	        {{ Form::text('tel','',['class' => 'form-control']) }}
	        <label for="address"><b>address</b></label>
	        {{ Form::text('address','',['class' => 'form-control']) }}
	        <label for="description"><b>Description</b></label>
	        {{ Form::text('Description','',['class' => 'form-control']) }}
	        <label for="education"><b>Education</b></label>
	        {{ Form::text('Education','',['class' => 'form-control']) }}
        	<label for="resume">Resume</label>
        	{{ Form::file('cover_image') }}

        	{{ Form::select('status', ['Er' => 'Employer', 'Ee' => 'Employee']) }}

        	{{ Form::submit('Update',['class' => 'btn btn-primary']) }} 
        {{ Form::close() }}
    </div>
    <div class="container signin">
        <p>Already have an account? <a href="#">Sign in</a>.</p>
    </div>
<br>
<div>

</div>

</body>
</html>