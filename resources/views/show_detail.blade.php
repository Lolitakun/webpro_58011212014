<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{ Html::style(('css/bootstrap.css')) }}
	{{ Html::script('js/jquery-3.3.1.min.js') }}
	{{ Html::script('js/boostrap.min.js') }}
</head>
<body>
	<h1 class="badge badge-primary">Owner Topic</h1>
  <img src="{{asset('/_.jpeg')}}" class="css-class" alt="profile Pic" height="200" width="200">
	<table class="table table-dark">
    	<thead class="thead-dark">
         	<tr>
            	<th scope="col">Name</th>
            	<th scope="col">Email</th>
            	<th scope="col">Telephone</th>
            	<th scope="col">Address</th>
            	<th scope="col">Description</th>
            	</tr>
        </thead>
        	<tbody>        
               	<tr>
                   	<td>{{ $owner[0]->hr_fullname }}</td>     
                   	<td>{{ $owner[0]->hr_email }}</td>
                   	<td>{{ $owner[0]->hr_tel }}</td>
                  	<td>{{ $owner[0]->hr_address }}</td>
                  	<td>{{ $owner[0]->hr_Description }}</td>
                </tr>                    
    	    </tbody>                     
	</table>
	<h1 class="badge badge-primary">Work Detail</h1>
	<table class="table table-dark">
    	<thead class="thead-dark">
         	<tr>
            	<th scope="col">Topic</th>
            	<th scope="col">Education</th>
            	<th scope="col">Saraly</th>
            	<th scope="col">Organization</th>
            	</tr>
        </thead>
        	<tbody>        
               	<tr>
                   	<td>{{ $work[0]->wl_head }}</td>     
                   	<td>{{ $work[0]->wl_Education }}</td>
                   	<td>{{ $work[0]->wl_Saraly }}</td>
                  	<td>{{ $work[0]->wl_Organization }}</td>
                </tr>                    
    	    </tbody>                     
	</table>
</body>
</html>
