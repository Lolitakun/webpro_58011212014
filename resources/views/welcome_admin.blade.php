<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8"> 	
    <title>Worker</title>
    {{ Html::style(('css/bootstrap.css')) }}
    {{ Html::script('js/jquery-3.3.1.min.js') }}
    {{ Html::script('js/boostrap.min.js') }}
</head>
<body>
	<div class="flex-center position-ref full-height">  
            @if(Session::has('Token'))            
                <form method="GET" action="WorkListController@show_admin">
                    <h1 align="center"><a href="WorkList">Work List</a></h1>
                </form>                    
                <form method="GET" action="WorkerController@index">   
                    <h1 align="center"><a href="Worker">Worker List</a></h1>
                </form>
                <form method="GET" action="HumanResourceController@index">
                    <h1 align="center"><a href="HR">Human Resource List</a></h1>               
                </form>
                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Topic</th>
                            <th scope="col">Education</th>
                            <th scope="col">Saraly</th>
                            <th scope="col">Organization</th>
                        </tr>
                    </thead>
                    <tbody>                        
                		@for($i=0;$i<count($blog);$i++)                              
                            <tr>
                                <td>{{ $blog[$i]->wl_head }}</td> 
                                <td>{{ $blog[$i]->wl_Education }}</td>
                                <td>{{ $blog[$i]->wl_Saraly }}</td>
                                <td>{{ $blog[$i]->wl_Organization }}</td>
                                <td>
                                <form method="GET" action=" {{ url('WorkListController@show') }}">
                                    <a href="TopicView/{{$blog[$i]->wl_id}}">View</a> </form>

                                <form method="GET" action="{{ url('/TopicEdit') }} ">
                                    <a href="TopicEdit/{{$blog[$i]->wl_id}}">Edit</a></form>

                                <form method="GET" action=" {{ url('WorkListController@destroy') }}">
                                    <a href="TopicDelete/{{$blog[$i]->wl_id}}">Delete</a> </form>
                                </td>                                
                            </tr>                    
                        </tbody>                 
                    @endfor
                </table>
                <div align="Center">
                {{ Form::open(['route' => 'logout_system']) }}
                    {{ Form::submit('Logout',['class' => 'btn btn-primary']) }}
                {{ Form::close()}}                       
            </div>
            @endif                      
        </div>
</body>
</html>