<!doctype html>
<html>
    <head>
        <meta charset="utf-8">   
        {{ Html::style(('css/bootstrap.css')) }}
        {{ Html::script('js/jquery-3.3.1.min.js') }}
        {{ Html::script('js/boostrap.min.js') }}   
        <style>
            #1{
              border: 5px solid black;             
              margin-right: 150px;
              margin-left: 150px;              
            }
        </style>     
    </head>
    <body>
        <h1 class="table table-dark">Hello Guest User :D </h1>
        <div class="table table-dark">
            @if(!Session::has('Token'))
                {{ Form::open(['route' => 'check_login']) }}
                    {{ Form::label('Username','Username')}}
                    {{ Form::text('Username','',['class' => 'form-control']) }}
                    {{ Form::label('Password','Password')}}
                    {{ Form::password('Password') }}
                    {{ Form::submit('Login',['class' => 'bt btn-primary'])}}
                    <a href="{{ url('register')}}">Register</a>
                {{ Form::close() }}
            @endif    
        </div>        
            <div id="#1" padding="70px">
                <h1 align="center">Work List</h1>
                <table class="table table-dark" >
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Topic</th>
                            <th scope="col">Education</th>
                            <th scope="col">Saraly</th>
                            <th scope="col">Organization</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($blog as $key => $b)
                        <tr>
                            <td>{{ $b->wl_head }}</td>     
                            <td>{{ $b->wl_Education }}</td>
                            <td>{{ $b->wl_Saraly }}</td>
                            <td>{{ $b->wl_Organization }}</td>
                        </tr>                    
                    </tbody>                     
                    @empty
                    <h2>        No Data!!       </h2>
                    @endforelse
                </table>     
            </div>                               
        
    </body>
</html>
