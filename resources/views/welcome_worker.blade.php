<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8"> 	
    <title>Worker</title>
    {{ Html::style(('css/bootstrap.css')) }}
    {{ Html::script('js/jquery-3.3.1.min.js') }}
    {{ Html::script('js/boostrap.min.js') }}
</head>
<body>
	<div class="flex-center position-ref full-height">  
            @if(Session::has('Token'))                          
                <h1 align="center">Work List</h1>
                <div align="center">
                {{ Form::open(['route' => 'check_login']) }}
                    {{ Form::label('findwork','Name')}}
                    {{ Form::text('work','',['class' => 'form-control']) }}                    
                    {{ Form::submit('Search',['class' => 'bt btn-primary'])}}                    
                {{ Form::close() }}
                </div>                
                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Topic</th>
                            <th scope="col">Education</th>
                            <th scope="col">Saraly</th>
                            <th scope="col">Organization</th>
                        </tr>
                    </thead>
                    <tbody>                        
                		@for($i=0;$i<count($blog);$i++)  
                            <form method="GET" action=" {{ url('WorkListController@show') }}">
                            <tr>
                                <td>{{ $blog[$i]->wl_head }}</td> 
                                <td>{{ $blog[$i]->wl_Education }}</td>
                                <td>{{ $blog[$i]->wl_Saraly }}</td>
                                <td>{{ $blog[$i]->wl_Organization }}</td>
                                <td>
                                    <a href="TopicView/{{$blog[$i]->wl_id}}">View</a>
                                </td>                                
                            </tr>                    
                        </tbody>
                    </form>                       
                    @endfor
                </table>
                <div align="Center">
                {{ Form::open(['route' => 'logout_system']) }}
                    {{ Form::submit('Logout',['class' => 'btn btn-primary']) }}
                {{ Form::close()}}                       
            </div>  
            @endif                      
        </div>
</body>
</html>