<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8"> 	
    <title>HR</title>
    {{ Html::style(('css/bootstrap.css')) }}
    {{ Html::script('js/jquery-3.3.1.min.js') }}
    {{ Html::script('js/boostrap.min.js') }}
</head>
<body>	
	<div class="flex-center position-ref full-height">  
            @if(Session::has('Token'))                     
            <div align="Center" >
                <a href="addjob/{{$owner}}">New Topic</a>
            </div>                     
            <form method="GET" action="WorkerController@index">   
                    <h1 align="center"><a href="Worker">Worker List</a></h1>
                </form>           
            @endif
                <h1 align="center">Work List</h1>
                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Topic</th>
                            <th scope="col">Education</th>
                            <th scope="col">Saraly</th>
                            <th scope="col">Organization</th>
                        </tr>
                    </thead>
                    <tbody>          
                		@for($i=0;$i<count($blog);$i++)                       
                            <tr>
                                <td>{{ $blog[$i]->wl_head }}</td>                                     
                                <td>{{ $blog[$i]->wl_Education }}</td>
                                <td>{{ $blog[$i]->wl_Saraly }}</td>
                                <td>{{ $blog[$i]->wl_Organization }}</td>
                                     
                            </tr>                    
                        </tbody>
                        @endfor
                </table>        
                @if(Session::has('Token'))                            
                <div align="Center">
                {{ Form::open(['route' => 'logout_system']) }}
                    {{ Form::submit('Logout',['class' => 'btn btn-primary']) }}
                {{ Form::close() }}         
                @endif                            
            </div> 
        </div>
</body>
</html>