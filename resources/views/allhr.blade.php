<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8"> 	
    <title>Worker</title>
    {{ Html::style(('css/bootstrap.css')) }}
    {{ Html::script('js/jquery-3.3.1.min.js') }}
    {{ Html::script('js/boostrap.min.js') }}
</head>
<body>
	<div class="flex-center position-ref full-height">
           
                <form method="GET" action="WorkListController@show_admin">
                    <h1 align="center"><a href="WorkList">Work List</a></h1>
                </form>                    
                <form method="GET" action="WorkerController@index">   
                    <h1 align="center"><a href="Worker">Worker List</a></h1>
                </form>
                <form method="GET" action="HumanResourceController@index">
                    <h1 align="center"><a href="HR">Human Resource List</a></h1>               
                </form>                 
                <table class="table table-dark">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Telephone</th>
                            <th scope="col">Address</th>
                            <th scope="col">Description</th>
                        </tr>
                    </thead>
                    @if(isset($hr))
                        <tbody>                        
                    		@for($i=0;$i<count($hr);$i++)                              
                                <tr>
                                    <td>{{ $hr[$i]->hr_fullname }}</td> 
                                    <td>{{ $hr[$i]->hr_tel }}</td>
                                    <td>{{ $hr[$i]->hr_address }}</td>
                                    <td>{{ $hr[$i]->hr_Description }}</td>
                                    <td>
                                    <form method="GET" action="TopicEdit">
                                        <a href="TopicEdit/{{$hr[$i]->w_id}}">Edit</a> </form>

                                    <form method="GET" action=" {{ url('WorkListController@destroy') }}">
                                        <a href="TopicDelete/{{$hr[$i]->w_id}}">Delete</a> </form>
                                    </td>                                
                                </tr>                    
                            </tbody>     
                            @endfor
                    @endif
                </table>              
                 <div align="Center">
                {{ Form::open(['route' => 'logout_system']) }}
                    {{ Form::submit('Logout',['class' => 'btn btn-primary']) }}
                {{ Form::close()}}                       
            </div>
        </div>
</body>
</html>