<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WorkListController@index');
Route::post('/login','WorkerController@check_login')->name('check_login');
Route::post('/logout','WorkerController@destroy')->name('logout_system');


Route::get('/register',function(){
	return view('new_register');
});
Route::post('register','WorkerController@store')->name('NewUser');

Route::get('/addjob/{id}',function($id){
	return view('addjob')->with('id',$id);
})->name('NewJob');
Route::post('/addjob','WorkListController@store')->name('NewJob');


//------------------------show detail---------------------
Route::get('/TopicView/{id}','WorkListController@show');
//---------------------------------------------------------

Route::post('/TopicEdit/{id}','WorkListController@update');

Route::get('/TopicEdit',function(){
	return view('edit_form');
});
Route::get('/TopicDelete/{id}','WorkListController@destroy');

Route::get('/Worker','WorkerController@index');
Route::get('/HR','HumanResourceController@index');
Route::get('/WorkList','WorkListController@show_admin');